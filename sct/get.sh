#!/bin/sh

PROGRAM_NAME="`basename $0`"

EXIT_SUCCESS=0
EXIT_FAILURE=1

v="$(curl -s "https://api.github.com/repos/faf0/sct/releases/latest" | \
  jq '.tag_name' | \
  tr -d '"')"

echo "getting asset..."
wget -q "https://github.com/faf0/sct/archive/refs/tags/$v.tar.gz" -O sct.tar.gz
[ $? -ne 0 ] \
  && echo "cannot download version $v" >&2 \
  && exit $EXIT_FAILURE
tar -xzf sct.tar.gz
mv sct*/ sct

exit $EXIT_SUCCESS
