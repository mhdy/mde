#!/bin/sh

PROGRAM_NAME="`basename $0`"

EXIT_SUCCESS=0
EXIT_FAILURE=1

v="$(curl -s "https://api.github.com/repos/burntsushi/ripgrep/releases/latest" | \
  jq '.tag_name' | \
  tr -d '"')"

echo "getting binary..."
wget -q "https://github.com/BurntSushi/ripgrep/releases/download/$v/ripgrep-$v-x86_64-unknown-linux-musl.tar.gz" -O rg.tar.gz
[ $? -ne 0 ] \
  && echo "cannot download version $v" >&2 \
  && exit $EXIT_FAILURE

tar --wildcards -xzf rg.tar.gz */rg */doc/rg.1
mv */rg */doc/rg.1 .

rm -rf rip*

exit $EXIT_SUCCESS
