/* user and group to drop privileges to */
static const char *user  = "mhdy";
static const char *group = "mhdy";

static const char *colorname[NUMCOLS] = {
  [INIT] =   "#000000",   /* after initialization */
  [INPUT] =  "#212121",   /* during input */
  [FAILED] = "#F44336",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;
