#!/bin/sh

PROGRAM_NAME="`basename $0`"

EXIT_SUCCESS=0
EXIT_FAILURE=1

v="$(curl -s "https://api.github.com/repos/sharkdp/fd/releases/latest" | \
  jq '.tag_name' | \
  tr -d '"')"

echo "getting binary..."
wget -q "https://github.com/sharkdp/fd/releases/download/$v/fd-$v-x86_64-unknown-linux-musl.tar.gz" -O fd.tar.gz
[ $? -ne 0 ] \
  && echo "cannot download version $v" >&2 \
  && exit $EXIT_FAILURE

tar --wildcards -xzf fd.tar.gz */fd */fd.1
mv */fd */fd.1 .

rm -rf fd-*

exit $EXIT_SUCCESS
