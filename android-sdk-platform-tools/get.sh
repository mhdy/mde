#!/bin/sh

PROGRAM_NAME="$(basename "$0")"

EXIT_SUCCESS=0
EXIT_FAILURE=1

echo "getting Android SDK Plateform Tools..."
wget "https://dl.google.com/android/repository/platform-tools-latest-linux.zip" \
  -O platform-tools-latest-linux.zip
[ $? -ne 0 ] \
  && echo "cannot download plateform-tools-latest-linux.zip" >&2 \
  && exit $EXIT_FAILURE

exit $EXIT_SUCCESS
