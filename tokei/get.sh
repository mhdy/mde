#!/bin/sh

PROGRAM_NAME="`basename $0`"

EXIT_SUCCESS=0
EXIT_FAILURE=1

v="$(curl -s "https://api.github.com/repos/XAMPPRocky/tokei/releases/latest" | \
  jq '.tag_name' | \
  tr -d '"')"

echo "getting binary..."
wget -q "https://github.com/XAMPPRocky/tokei/releases/download/$v/tokei-x86_64-unknown-linux-musl.tar.gz" -O tokei.tar.gz
[ $? -ne 0 ] \
  && echo "cannot download version $v" >&2 \
  && exit $EXIT_FAILURE

tar -zxf tokei.tar.gz

rm -rf tokei.tar.gz

exit $EXIT_SUCCESS
