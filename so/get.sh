#!/bin/bash

PROGRAM_NAME="`basename $0`"

EXIT_SUCCESS=0
EXIT_FAILURE=1

v="$(curl -s "https://api.github.com/repos/samtay/so/releases/latest" | \
  jq '.tag_name' | \
  tr -d 'v"')"

echo "getting asset..."
wget -q "https://github.com/samtay/so/releases/download/v$v/so-x86_64-unknown-linux-musl.tar.gz" -O so.tar.gz
[ $? -ne 0 ] \
  && echo "cannot download version $v" >&2 \
  && exit $EXIT_FAILURE
tar -xzf so.tar.gz

exit $EXIT_SUCCESS
