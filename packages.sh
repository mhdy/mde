#!/bin/bash

PROGRAM_NAME="$(basename "$0")"

EXIT_SUCCESS=0
EXIT_FAILURE=1

# run as root
[ "$EUID" -ne 0 ] \
  && echo "root required to execute this script." >&2 \
  && exit $EXIT_FAILURE
usr=$SUDO_USER
grp=$(id $usr | cut -d ' ' -f 2 | sed 's/.*(\(.*\))/\1/g')
home="$(grep $usr /etc/passwd | cut -d ':' -f 6)"

echo "#################################"
echo "# PACKAGES FROM DEBIAN ##########"
echo "#################################"

apt-get update && apt-get install -y \
  alsa-utils \
  arandr \
  autoconf \
  bc \
  bluetooth \
  brightnessctl \
  build-essential \
  curl \
  dkms \
  dunst \
  edid-decode \
  exuberant-ctags \
  firefox-esr \
  fontconfig \
  genisoimage \
  gimp \
  git \
  gstreamer1.0-{libav,plugins-bad,plugins-base,plugins-good,plugins-ugly,vaapi} \
  heimdall-flash \
  htop \
  imagemagick \
  itools \
  jmtpfs \
  jq \
  libfribidi-dev \
  libitl-dev \
  libncursesw5-dev \
  libnotify-bin \
  libreadline-dev \
  libreoffice-{writer,calc,gnome} \
  libssl-dev \
  libx11-dev \
  libxft-dev \
  libxinerama-dev \
  libxrandr-dev \
  linux-headers-amd64 \
  maim \
  mesa-utils \
  moreutils \
  mpv \
  neofetch \
  newsboat \
  nmap \
  ntfs-3g \
  ntp \
  p7zip-full \
  pigz \
  pkg-config \
  plymouth \
  poppler-utils \
  pulseaudio \
  pulseaudio-module-bluetooth \
  pulsemixer \
  rar \
  rfkill \
  rsync \
  screen \
  sxiv \
  transmission-{cli,gtk} \
  tree \
  udisks2 \
  unclutter \
  unrar \
  unzip \
  valgrind \
  vim \
  vim-nox \
  w3m \
  whois \
  xcalib \
  xclip \
  xdg-utils \
  xdotool \
  xinput \
  xorg \
  xwallpaper \
  zathura \
  zathura-{pdf-poppler,djvu,cb,ps} \
  zip \
  zstd

echo "debian packages installed successfully."

echo "#################################"
echo "# DOCKER ########################"
echo "#################################"

apt-get install -y ca-certificates curl gnupg lsb-release
sudo mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor --yes --output /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" \
  > /etc/apt/sources.list.d/docker.list
apt-get update && apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

usermod -aG docker $usr

if ! grep -q "$usr:1000:1" /etc/subuid
then
  mv /etc/subuid /etc/subuid.orig
  echo "$usr:1000:1" | cat - /etc/subuid.orig > /etc/subuid
fi
if ! grep -q "$grp:1000:1" /etc/subgid
then
  mv /etc/subgid /etc/subgid.orig
  echo "$grp:1000:1" | cat - /etc/subgid.orig > /etc/subgid
fi

cat > /etc/docker/daemon.json << EOF
{
  "userns-remap": "$usr"
}
EOF

systemctl enable docker.service

echo "Docker installed successfully."

# echo "#################################"
# echo "# KVM/QEMU/VIRT #################"
# echo "#################################"

# apt-get install -y qemu-system-x86 \
#   libvirt-clients libvirt-daemon-system virt-manager spice-vdagent
# usermod -aG libvirt $usr

# echo "KVM/QEMU/VIRT installed successfully."

echo "#################################"
echo "# VIRTUALBOX ####################"
echo "#################################"

wget -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | gpg --dearmor --yes --output /etc/apt/keyrings/oracle-virtualbox-2016.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/oracle-virtualbox-2016.gpg] https://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib" \
  > /etc/apt/sources.list.d/virtualbox.list
apt-get update && apt-get -y install virtualbox-7.0
usermod -aG vboxusers $usr

echo "Virtualbox installed successfully."

echo "#################################"
echo "# MISC ##########################"
echo "#################################"

# required for screen brightness control
usermod -aG video $usr

# grub
sed -i \
  -e 's/GRUB_TIMEOUT=[0-9]\+/GRUB_TIMEOUT=0/g' \
  -e 's/GRUB_CMDLINE_LINUX_DEFAULT=.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"/g' \
  -e 's/#\?GRUB_GFXMODE=.*/GRUB_GFXMODE=1920x1080/g' \
  -e 's/#GRUB_TERMINAL=console/GRUB_TEMRINAL=console/g' \
  /etc/default/grub
update-grub
plymouth-set-default-theme -R details
update-grub

echo "installation succeeded."

exit $EXIT_SUCCESS
