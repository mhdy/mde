#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>

#define PROGRAM_NAME "battery-daemon"

#define NOTIFY_PROGRAM "Battery Daemon"

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

static const char *batteries[] = {
  "/sys/class/power_supply/BAT0/",
  "/sys/class/power_supply/BAT1/",
  NULL
};

static const char *ac = "/sys/class/power_supply/AC/online";

#define FREQ 5
#define CRITICAL_LEVEL 5
#define LOW_LEVEL 15
#define HIGH_LEVEL 85

static const char *action_critical[] = {
  "notify-send", "-u", "critical", NOTIFY_PROGRAM,
  "The battery capacity is critical (" STR(CRITICAL_LEVEL) "% remaining).\n"
  "Consider plugging the charger.",
  NULL
};
static const char *action_low[] = {
  "notify-send", NOTIFY_PROGRAM,
  "The battery capacity is low (" STR(LOW_LEVEL) "% remaining).\n"
  "Consider plugging the charger.",
  NULL
};
static const char *action_high[] = {
  "notify-send", NOTIFY_PROGRAM,
  "The battery capacity is high.\nConsider unplugging the charger.",
  NULL
};
static const char *action_full[] = {
  "notify-send", NOTIFY_PROGRAM,
  "The battery capacity is full.\nConsider unplugging the charger.",
  NULL
};
static const char *action_error[] = {
  "notify-send", NOTIFY_PROGRAM,
  "Cannot get battery information.\n"
  NOTIFY_PROGRAM " terminated.",
  NULL
};

int
get_status(void)
{
  int status;
  FILE *fp;

  fp = fopen(ac, "r");
  if (!fp)
    return -1;
  fscanf(fp, "%d", &status);
  fclose(fp);
  if (status != 0 && status != 1)
    return -1;
  return status;
}

int
get_capacity(void)
{
  FILE *fp;
  float fe, c, a = 0, b = 0;
  char path[BUFSIZ];
  int i;

  for (i = 0; batteries[i] != NULL; i++)
  {
    /* read energy */
    strcpy(path, batteries[i]);
    strcat(path, "energy_full");
    fp = fopen(path, "r");
    if (!fp)
      return -1;
    fscanf(fp, "%f", &fe);
    fclose(fp);
    /* read capacity */
    strcpy(path, batteries[i]);
    strcat(path, "capacity");
    fp = fopen(path, "r");
    if (!fp)
      return -1;
    fscanf(fp, "%f", &c);
    fclose(fp);
    /* do math */
    a += fe * c;
    b += fe;
  }
  return (int) ceil(a / b);
}

void
exec_action(const char **action)
{
	if (fork() == 0)
  {
		setsid();
		execvp(action[0], (char **)action);
		fprintf(stderr, "%s: execvp %s", PROGRAM_NAME, action[0]);
		perror(" failed");
		exit(EXIT_SUCCESS);
	}
}

void
run(void)
{
  int status, capacity;
  int critical, low, high, full;

  critical = low = high = full = 0;
  while (1)
  {
    status = get_status();
    capacity = get_capacity();
    if (status == -1 || capacity == -1)
    {
      exec_action(action_error);
      exit(EXIT_FAILURE);
    }
    if (status == 1)
    {
      if (capacity >= HIGH_LEVEL && high == 0)
      {
        exec_action(action_high);
        high = 1;
      }
      if (capacity == 100 && full == 0)
      {
        exec_action(action_full);
        full = 1;
      }
      low = capacity <= LOW_LEVEL;
      critical = capacity <= CRITICAL_LEVEL;
    }
    else
    {
      if (capacity <= LOW_LEVEL && low == 0)
      {
        exec_action(action_low);
        low = 1;
      }
      if (capacity <= CRITICAL_LEVEL && critical == 0)
      {
        exec_action(action_critical);
        critical = 1;
      }
      full = 0;
      high = capacity >= HIGH_LEVEL;
    }
    sleep(FREQ);
  }
}

static void
battery_daemon(void)
{
  pid_t pid;
  int x;
  /* first fork */
  pid = fork();
  if (pid < 0)
    exit(EXIT_FAILURE);
  if (pid > 0)
    exit(EXIT_SUCCESS);
  if (setsid() < 0)
    exit(EXIT_FAILURE);
  /* second fork */
  pid = fork();
  if (pid < 0)
    exit(EXIT_FAILURE);
  if (pid > 0)
    exit(EXIT_SUCCESS);
  umask(0);
  chdir("/");
  for (x = sysconf(_SC_OPEN_MAX); x >= 0; x--)
    close(x);
}

int
main (int argc, char **argv)
{
  battery_daemon();
  run();
  return EXIT_SUCCESS;
}
