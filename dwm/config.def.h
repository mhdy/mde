/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx = 1; /* border pixel of windows */
static const unsigned int gappx = 10; /* gaps between windows */
static const unsigned int snap = 32; /* snap pixel */
static const int showbar = 1; /* 0 means no bar */
static const int topbar = 1; /* 0 means bottom bar */
static const char *fonts[] = {
  "Roboto:size=11",
  "Open Sans:size=11",
  "Material Design Icons:size=12",
};
static const char dmenufont[] = "Roboto:size=11";

#include "themes/ghyam.h"
static const char *colors[][3] = {
  /*               fg         bg         border   */
  [SchemeNorm] = { col_norm_fg, col_norm_bg, col_norm_border },
  [SchemeSel]  = { col_sel_fg, col_sel_bg, col_sel_border },
};

/* layout(s) */
static const float mfact = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster = 1; /* number of clients in master area */
static const int resizehints = 1; /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[T]", tile }, /* first entry is default */
	{ "[M]", monocle },
	{ "[F]", NULL }, /* no layout function means floating behavior */
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "vm", "email", "web" };

static const Rule rules[] = {
  /* class, instance, title, tags mask, isfloating, monitor */
  { "Firefox", NULL, NULL, 1 << 8, 0, -1 },
  { "Mmail", NULL, NULL, 1 << 7, 0, -1 },
  { "VirtualBox", NULL, NULL, 1 << 6, 0, -1 },
  { "Virt-manager", NULL, NULL, 1 << 7, 0, -1 },
  { "Sxiv", NULL, NULL, 0, 1, -1 },
  { "Pulsemixer", NULL, NULL, 0, 1, -1 },
  { "ffplay", "ffplay", "webcam", 0, 1, -1 },
  /* scratchpads */
  { NULL, "Spst", NULL, SPTAG(0), 1, -1 },
  { NULL, "Splf", NULL, SPTAG(1), 1, -1 },
};

/* scratchpads */
const char *spst[] = { "st", "-n", "Spst", "-g", "120x34", NULL };
const char *splf[] = { "st", "-n", "Splf", "-g", "120x34", "-e", "lf_run", NULL };
typedef struct {
  const char *name;
  const void *cmd;
} Sp;
static const Sp scratchpads[] = {
  { "spst", spst },
  { "splf", splf },
};

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = {
  "dmenu_run", "-p", "> ", "-m", dmenumon, "-fn", dmenufont,
  "-nb", col_norm_bg, "-nf", col_norm_fg, "-sb", col_sel_bg, "-sf", col_sel_fg,
  topbar ? NULL : "-b", NULL
};

static const char *diskutil[] = { "dmenu-diskutil", NULL };
static const char *powerutil[] = { "dmenu-powerutil", NULL };
static const char *wifiutil[] = { "dmenu-wifiutil", NULL };
static const char *bluetoothutil[] = { "dmenu-bluetoothutil", NULL };
static const char *screenshotutil[] = { "dmenu-screenshotutil", NULL };
static const char *vmutil[] = { "dmenu-vmutil", NULL };
static const char *monitorutil[] = { "dmenu-monitorutil", NULL };
static const char *recordingutil[] = { "dmenu-recordingutil", NULL };
static const char *passutilquick[] = { "dmenu-passutil", "quick", NULL };
static const char *passutilinteractive[] = { "dmenu-passutil", "interactive", NULL };

static const char *voltoggle[] = { "dwm-setvolume", "toggle", NULL };
static const char *voldown[] = { "dwm-setvolume", "down", NULL };
static const char *volup[] = { "dwm-setvolume", "up", NULL };
static const char *mictoggle[] = { "dwm-setmicvolume", "toggle", NULL };
static const char *micdown[] = { "dwm-setmicvolume", "down", NULL };
static const char *micup[] = { "dwm-setmicvolume", "up", NULL };
static const char *brightnessdown[] = { "dwm-setbrightness", "down", NULL };
static const char *brightnessup[] = { "dwm-setbrightness", "up", NULL };
static const char *brightness5[] = { "dwm-setbrightness", "5", NULL };
static const char *brightness75[] = { "dwm-setbrightness", "75", NULL };
static const char *nightlighttoggle[] = { "dwm-setnightlight", NULL };
static const char *capslock[] = { "dwm-capslock", NULL };
static const char *monitorauto[] = { "mrandr", "auto", NULL };

static const char *openterm[] = { "dwm-openterm", NULL };
static const char *lockscreen[] = { "slock", NULL };
static const char *filemanager[] = { "st", "-e", "lf_run", NULL };
static const char *pulsemixer[] = { "st", "-c", "Pulsemixer", "-e", "pulsemixer", NULL };
static const char *newsboat[] = { "st", "-c", "Newsboat", "-e", "newsboat", "-r", NULL };
static const char *mmail[] = { "st", "-c", "Mmail", "-e", "mmail", NULL };
static const char *webbrowser[] = { "firefox", "-P", "default-esr", NULL };
static const char *webprofiles[] = { "dmenu-browser", NULL };
static const char *xf86fav[] = { "dwm-xf86fav", NULL };

static const char *suspend[] = { "systemctl", "suspend", NULL };
static const char *hibernate[] = { "systemctl", "hibernate", NULL };
static const char *poweroff[] = { "systemctl", "poweroff", NULL };
static const char *reboot[] = { "systemctl", "reboot", NULL };

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* key bindings */
static Key keys[] = {
  /* modifier             key           function       argument */
  /* layouts */
  { MODKEY|Mod1Mask,      XK_t,         setlayout,     { .v = &layouts[0]} },
  { MODKEY|Mod1Mask,      XK_m,         setlayout,     { .v = &layouts[1]} },
  { MODKEY|Mod1Mask,      XK_f,         setlayout,     { .v = &layouts[2]} },
  /* views and tags */
  { MODKEY,               XK_0,         view,          { .ui = ~0 } },
  { MODKEY|ShiftMask,     XK_0,         tag,           { .ui = ~0 } },
  TAGKEYS(                XK_1,                        0)
  TAGKEYS(                XK_2,                        1)
  TAGKEYS(                XK_3,                        2)
  TAGKEYS(                XK_4,                        3)
  TAGKEYS(                XK_5,                        4)
  TAGKEYS(                XK_6,                        5)
  TAGKEYS(                XK_7,                        6)
  TAGKEYS(                XK_8,                        7)
  TAGKEYS(                XK_9,                        8)
  { MODKEY,               XK_h,         viewprev,      { 0 } },
  { MODKEY,               XK_l,         viewnext,      { 0 } },
  { MODKEY|ShiftMask,     XK_h,         tagtoprev,     { 0 } },
  { MODKEY|ShiftMask,     XK_l,         tagtonext,     { 0 } },
  /* ui */
  { MODKEY,               XK_b,         togglebar,     {0} },
  { MODKEY,               XK_k,         focusstack,    { .i = -1 } },
  { MODKEY,               XK_j,         focusstack,    { .i = +1 } },
  { MODKEY|ShiftMask,     XK_k,         movestack,     { .i = -1 } },
  { MODKEY|ShiftMask,     XK_j,         movestack,     { .i = +1 } },
  { MODKEY|ShiftMask,     XK_equal,     incnmaster,    { .i = +1 } },
  { MODKEY,               XK_minus,     incnmaster,    { .i = -1 } },
  { MODKEY,               XK_Left,      setmfact,      { .f = -0.05 } },
  { MODKEY,               XK_Right,     setmfact,      { .f = +0.05 } },
  { MODKEY,               XK_Down,      setcfact,      { .f = -0.25 } },
  { MODKEY,               XK_Up,        setcfact,      { .f = +0.25 } },
  { MODKEY,               XK_equal,     setcfact,      { .f =  0.0 } },
  { MODKEY,               XK_backslash, zoom,          { 0 } },
  { MODKEY,               XK_Tab,       view,          { 0 } },
  { MODKEY,               XK_q,         killclient,    { 0 } },
  { MODKEY,               XK_End,       quit,          { 0 } },
  /* monitors */
  { MODKEY,               XK_comma,     focusmon,      { .i = -1 } },
  { MODKEY,               XK_period,    focusmon,      { .i = +1 } },
  { MODKEY|ShiftMask,     XK_comma,     tagmon,        { .i = -1 } },
  { MODKEY|ShiftMask,     XK_period,    tagmon,        { .i = +1 } },
  /* programs */
  { MODKEY,               XK_d,         spawn,         { .v = dmenucmd } },
  { MODKEY,               XK_Return,    spawn,         { .v = openterm } },
  { MODKEY,               XK_Escape,    spawn,         { .v = lockscreen } },
  { MODKEY,               XK_f,         spawn,         { .v = filemanager } },
  { MODKEY,               XK_a,         spawn,         { .v = pulsemixer } },
  { MODKEY,               XK_w,         spawn,         { .v = webbrowser } },
  { MODKEY|ShiftMask,     XK_w,         spawn,         { .v = webprofiles } },
  { MODKEY,               XK_v,         spawn,         { .v = vmutil } },
  { MODKEY,               XK_m,         spawn,         { .v = mmail } },
  { MODKEY,               XK_n,         spawn,         { .v = newsboat } },
  /* scratchpads */
  { MODKEY|ShiftMask,     XK_Return,    togglescratch, { .ui = 0 } },
  { MODKEY|ShiftMask,     XK_f,         togglescratch, { .ui = 1 } },
  /* commands */
  { 0,                    XK_Print,     spawn,         { .v = screenshotutil } },
  { MODKEY,               XK_r,         spawn,         { .v = recordingutil } },
  { MODKEY,               XK_x,         spawn,         { .v = passutilquick } },
  { MODKEY|ShiftMask,     XK_x,         spawn,         { .v = passutilinteractive } },
  { MODKEY|ControlMask,   XK_slash,     spawn,         { .v = diskutil } },
  { MODKEY|ControlMask,   XK_w,         spawn,         { .v = wifiutil } },
  { MODKEY|ControlMask,   XK_b,         spawn,         { .v = bluetoothutil } },
  { MODKEY|ControlMask,   XK_End,       spawn,         { .v = powerutil } },
  { MODKEY|ControlMask,   XK_s,         spawn,         { .v = suspend } },
  { MODKEY|ControlMask,   XK_h,         spawn,         { .v = hibernate } },
  { MODKEY|ControlMask,   XK_p,         spawn,         { .v = poweroff } },
  { MODKEY|ControlMask,   XK_r,         spawn,         { .v = reboot } },

  /* XF86 keys */

  { 0, XF86XK_AudioMute, spawn, { .v = voltoggle } },
  { 0, XF86XK_AudioLowerVolume, spawn, { .v = voldown } },
  { 0, XF86XK_AudioRaiseVolume, spawn, { .v = volup } },
  { 0, XF86XK_AudioMicMute, spawn, { .v = mictoggle } },
  { Mod1Mask, XF86XK_AudioLowerVolume, spawn, { .v = micdown } },
  { Mod1Mask, XF86XK_AudioRaiseVolume, spawn, { .v = micup } },

  { 0, XF86XK_MonBrightnessDown, spawn, { .v = brightnessdown } },
  { 0, XF86XK_MonBrightnessUp, spawn, { .v = brightnessup } },
  { Mod1Mask, XF86XK_MonBrightnessDown, spawn, { .v = brightness5 } },
  { Mod1Mask, XF86XK_MonBrightnessUp, spawn, { .v = brightness75 } },
  { Mod1Mask, XF86XK_Display, spawn, { .v = nightlighttoggle } },
  { Mod1Mask, XK_F7, spawn, { .v = nightlighttoggle } },

  { 0, XK_Caps_Lock, spawn, { .v = capslock } },

  { ControlMask, XF86XK_Display, spawn, { .v = monitorutil } },
  { ControlMask, XK_F11, spawn, { .v = monitorutil } },
  { 0, XF86XK_Display, spawn, { .v = monitorauto } },
  { 0, XK_F7, spawn, { .v = monitorauto } },
  { 0, XF86XK_Favorites, spawn, { .v = xf86fav } },
  { 0, XK_F11, spawn, { .v = xf86fav } },

  /* { 0, XF86XK_Tools, spawn, { .v = settings } }, */
  /* { 0, XF86XK_WLAN, spawn, { .v =  } }, */
  /* { 0, XF86XK_Bluetooth, spawn, { .v =  } }, */
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
  /* click,       mask,             button,  function,       argument */
  { ClkTagBar,    0,                Button1, view,           {0} },
  { ClkTagBar,    ControlMask,      Button1, toggleview,     {0} },
  { ClkTagBar,    MODKEY|ShiftMask, Button1, tag,            {0} },
  { ClkLtSymbol,  0,                Button1, setlayout,      {0} },
  { ClkClientWin, MODKEY,           Button1, movemouse,      {0} },
  { ClkClientWin, MODKEY,           Button2, togglefloating, {0} },
  { ClkClientWin, MODKEY,           Button3, resizemouse,    {0} },
};
