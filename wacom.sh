#!/bin/sh

# This script sets up my wacom intuos to be used as a writing tablet in a
# multiple monitor set up.

PROGRAM_NAME="`basename $0`"

EXIT_SUCCESS=0
EXIT_FAILURE=1

# exit if no connected device
[ -z "$(xsetwacom list devices)" ] \
  && exit $EXIT_SUCCESS

# get IDs
penid=$(xsetwacom --list devices | \
  grep 'Pen' | \
  sed -e 's/[[:space:]]\+/ /g' -e 's/.*id: \(.*\) type: .*/\1/g')
padid=$(xsetwacom --list devices | \
  grep 'Pad' | \
  sed -e 's/[[:space:]]\+/ /g' -e 's/.*id: \(.*\) type: .*/\1/g')

# adapt to monitor
xrandr --listmonitors | grep -qs 'DP-1-2' && {
  xsetwacom set $penid MapToOutput DP-1-2
  xsetwacom set $padid MapToOutput DP-1-2
}

# set buttons
xsetwacom set $padid Button 3 "key p"
xsetwacom set $padid Button 1 "key +shift e -shift"
xsetwacom set $padid Button 8 "key +ctrl z -ctrl"
xsetwacom set $padid Button 9 "key +ctrl y -ctrl"

exit $EXIT_SUCCESS
