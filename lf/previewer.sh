#!/bin/sh

PROGRAM_NAME="(basename "$0")"

EXIT_SUCCESS=0
EXIT_FAILURE=1

f="$1" # path
h="$2" # height of the preview pane

fmime="$(file -b --mime-type "$f")"
nl=50

print_header() {
  f="$1"
  finfo="$(ls -lh "$f")"
  fperms="$(echo "$finfo" | cut -d ' ' -f 1 | sed 's/^.//g')"
  fpermsoctal="$(stat -c '%a' "$f")"
  fowner="$(echo "$finfo" | cut -d ' ' -f 3)"
  fgroup="$(echo "$finfo" | cut -d ' ' -f 4)"
  fsize="$(echo "$finfo" | cut -d ' ' -f 5)"
  cat << EOF
Path:        $f
Permissions: $fpermsoctal $fperms
Owner:       $fowner
Group:       $fgroup
Size:        $fsize
Type:        $fmime
EOF
}

[ -f "$f" ] && print_header "$f" && printf "\n-------\n\n"

case "$fmime" in
  text/*) head -n $nl "$f" ;;
  application/json) jq -aMr --indent 4 '' "$f" | head -n $nl ;;
  application/pdf) pdftotext "$f" - | head -n $nl  ;;
  image/*)
    width=$(identify -format '%w' "$f")
    height=$(identify -format '%h' "$f")
    echo "Image"
    echo "Height: $height"
    echo "Width:  $width"
    ;;
  video/*)
    echo "Video"
    ;;
  application/x-pie-executable) echo 'Binary' ;;
  inode/symlink) echo "$f -> $(realpath "$f")" ;;
  inode/directory) LC_COLLATE=C ls -aFhHlq -1 --group-directories-first --color=always ;;
esac

exit $EXIT_SUCCESS
