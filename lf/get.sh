#!/bin/sh

PROGRAM_NAME="`basename $0`"

EXIT_SUCCESS=0
EXIT_FAILURE=1

v="$(curl -s "https://api.github.com/repos/gokcehan/lf/releases/latest" | \
  jq '.tag_name' | \
  tr -d '"')"

echo "getting asset..."
wget -q "https://github.com/gokcehan/lf/releases/download/$v/lf-linux-amd64.tar.gz" -O lf.tar.gz
[ $? -ne 0 ] \
  && echo "cannot download version $v" >&2 \
  && exit $EXIT_FAILURE
tar -xzf lf.tar.gz

echo "getting man page..."
wget -q "https://raw.githubusercontent.com/gokcehan/lf/$v/lf.1" -O lf.1

exit $EXIT_SUCCESS
