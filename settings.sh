#!/bin/bash

PROGRAM_NAME="$(basename "$0")"

EXIT_SUCCESS=0
EXIT_FAILURE=1

# run as root
[ "$EUID" -ne 0 ] \
  && echo "root required to execute this script." >&2 \
  && exit $EXIT_FAILURE

###
### PULSEAUDIO
###

grep -q '^load-module module-switch-on-connect' /etc/pulse/default.pa \
  || echo 'load-module module-switch-on-connect' >> /etc/pulse/default.pa

###
### NETWORKING
###

# ufw
echo 'installing and configuring ufw...'
apt-get install -y ufw
ufw enable
ufw default allow outgoing
ufw default deny incoming
ufw logging off

# iwd
apt-get update && apt-get install -y iwd
echo 'disabling wpa supplicant...'
systemctl disable wpa_supplicant
echo 'disabling networking...'
systemctl disable networking
mv /etc/network/interfaces /etc/network/interfaces.bak
touch /etc/network/interfaces
chmod 644 /etc/network/interfaces
echo 'enabling iwd...'
systemctl enable iwd
echo 'enabling systemd-networkd and systemd-resolved'
systemctl enable systemd-networkd
systemctl enable systemd-resolved
ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf

mkdir -p /etc/iwd/

cat > /etc/iwd/main.conf << EOF
[General]
EnableNetworkConfiguration=True

[Network]
NameResolvingService=systemd
EOF

cat > /etc/systemd/network/20-wired.network << EOF
[Match]
Name=en* eth*

[Network]
DHCP=yes
EOF

cat > /etc/systemd/network/21-wireless.network << EOF
[Match]
Name=wl*

[Network]
DHCP=yes
EOF
