#!/bin/sh

PROGRAM_NAME="$(basename "$0")"

EXIT_SUCCESS=0
EXIT_FAILURE=1

# comment with `#'
paths="
android-sdk-platform-tools
bin
dmenu
dotfiles
dwm
fd
fzf
lf
mblocks
rg
sct
so
st
tokei
slock
"

for path in $(echo "$paths" | grep -v '^#')
do
  printf "\n\n"
  cd "$path"
  make clean install
  make clean
  cd ..
  sleep 5
done

exit $EXIT_SUCCESS
