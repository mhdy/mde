# MDE - M's Desktop Environment

This repo contains the desktop environment and the softwares I use on top of a fresh
minimal Debian installation (with no desktop environment) on my ThinkPad T470
laptop.
Since all the tools I use are dependent on one another, I opted for aggregating
all of them into one git repo.

![Screenshot 1](screenshots/screenshot.png)

This repo contains the following:

- [dwm](https://dwm.suckless.org/) window manager 
- [st](https://st.suckless.org/) (simple terminal)
- [dmenu](https://tools.suckless.org/dmenu/) X menu
- [slock](https://tools.suckless.org/slock/) X display locker
- [lf](https://github.com/gokcehan/lf) file manager
- dunst notification daemon
- sxiv image viewer
- zathura pdf (and other formats) viewer

as well as many other programs I use.

These are some noticeable features:

- iwd is used as a network manager instead of wpa\_supplicant (which is disabled).
- Wifi and bluetooth are controlled via iwctl and bluetoothctl
- Volumes management (mount / unmount / eject) is done via udisksctl (udisks2 package)
  and dmenu (see [dmenu-diskutil](dmenu/scripts/dmenu-diskutil) script)
- maim is used for screenshots (whole screen and by region)
- See the dwm key bindings at [dwm/config.def.h](dwm/config.def.h)
- All applied patches to the suckless utilities are included in the patches
  directories
- ThinkPad T470 XF86 key bindings are all working as expected

The [packages script](packages.sh) installs all the required Debian packages 
to get the desktop environment and suckless tools working properly. 
It also installs and sets up docker, as well as virtualbox or KVM/QEMU/VIRT
(comment the corresponding section in the script).
For wireless networking, I use iwd instead of wpa\_supplicant.

## Requirements

Minimal Debian instance with no desktop environment.

## Installation

The following installation guide is tested on Debian 11.

1. Install sudo and git, and add the main user if not done while installing Debian, as follows:

```
# apt-get update && apt-get install sudo git
# adduser USER
  ...
# usermod -aG sudo USER
```

2. Log out and log in as USER.

3. Clone the repository:

```
$ git clone https://gitlab.com/mhdy/mde.git
$ cd mde
```

4. Install the required packages and set up the system (networking, etc.): 

```
$ sudo ./packages.sh
$ sudo ./settings.sh
```

5. Reboot the system using `$ systemctl reboot`.

6. If you have a ThinkPad T470, than execute `$ sudo ./thinkpad.sh` and reboot the system using `$ systemctl reboot`

7. Finally, install the programs and the configurations:

```
$ ./post.sh
```

## License

MIT.
