use crate::block::Block;
#[allow(unused_imports)]
use crate::block::BlockType::{Interval, Once, Signal};
#[allow(unused_imports)]
use crate::block::CommandType::{Function, Shell};

use crate::blocks::cpu::cpu_usage;
use crate::blocks::datetime::current_time;
// use crate::blocks::memory::memory_usage;

pub const SEPARATOR: &str = " | ";
pub const PREFIX: &str = " ";
pub const SUFFIX: &str = " ";

pub const BLOCKS: &[Block] = &[
    Block {
        kind: Signal(5),
        command: Shell(&["mblocks-freeblock"]),
        prefix: "",
        suffix: "",
    },
    Block {
        kind: Signal(6),
        command: Shell(&["mblocks-capslock"]),
        prefix: "",
        suffix: "",
    },
    Block {
        kind: Interval(1),
        command: Function(cpu_usage),
        prefix: "󰻠 ",
        suffix: "%",
    },
    Block {
        kind: Interval(10),
        command: Shell(&["mblocks-memory"]),
        prefix: "󰍛 ",
        suffix: "",
    },
    Block {
        kind: Interval(30),
        command: Shell(&["mblocks-disk"]),
        prefix: "",
        suffix: "",
    },
    Block {
        kind: Signal(4),
        command: Shell(&["mblocks-brightness"]),
        prefix: "",
        suffix: "",
    },
    Block {
        kind: Signal(3),
        command: Shell(&["mblocks-microphone"]),
        prefix: "",
        suffix: "",
    },
    Block {
        kind: Signal(2),
        command: Shell(&["mblocks-volume"]),
        prefix: "",
        suffix: "",
    },
    Block {
        kind: Interval(5),
        command: Shell(&["mblocks-ethernet"]),
        prefix: "󰈀 ",
        suffix: "",
    },
    Block {
        kind: Interval(5),
        command: Shell(&["mblocks-wifi"]),
        prefix: "",
        suffix: "",
    },
    Block {
        kind: Interval(5),
        command: Shell(&["mblocks-bluetooth"]),
        prefix: "󰂯 ",
        suffix: "",
    },
    Block {
        kind: Interval(30),
        command: Shell(&["mblocks-battery"]),
        prefix: "",
        suffix: "",
    },
    Block {
        kind: Interval(1800),
        command: Shell(&["date", "+%a, %b %d %Y"]),
        prefix: "󰃭 ",
        suffix: "",
    },
    Block {
        kind: Interval(1800),
        command: Shell(&["mblocks-hijri-date"]),
        prefix: "󰃭 ",
        suffix: "",
    },
    Block {
        kind: Interval(30),
        command: Function(current_time),
        prefix: "󰅐 ",
        suffix: "",
    },
    Block {
        kind: Once,
        command: Shell(&["whoami"]),
        prefix: "󰀄 ",
        suffix: "",
    },
];
