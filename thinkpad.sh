#!/bin/bash

PROGRAM_NAME="`basename $0`"

EXIT_SUCCESS=0
EXIT_FAILURE=1

[ "$EUID" -ne 0 ] \
  && echo "root required to execute this script." >&2 \
  && exit $EXIT_FAILURE

apt-get update

apt-get install -y \
  firmware-iwlwifi intel-microcode firmware-misc-nonfree

apt-get install -y --no-install-recommends tlp acpi-call-dkms

echo 'options iwlwifi enable_ini=N' > /etc/modprobe.d/iwlwifi.conf
/sbin/update-initramfs -u

mkdir -p /etc/X11/xorg.conf.d/

# touchpad
cat > /etc/X11/xorg.conf.d/30-touchpad.conf << EOF
Section "InputClass"
  Identifier "thinkpad touchpad"
  Driver "libinput"
  MatchIsTouchpad "on"
  Option "Tapping" "on"
  Option "AccelSpeed" "0.7"
EndSection
EOF

# trackpad
cat > /etc/X11/xorg.conf.d/31-trackpoint.conf << EOF
Section "InputClass"
  Identifier "thinkpad trackpoint"
  Driver "libinput"
  MatchIsPointer "on"
  Option "AccelSpeed" "0.7"
EndSection
EOF

echo "the thinkpad is set up successfully and the machine requires reboot"

exit $EXIT_SUCCESS
