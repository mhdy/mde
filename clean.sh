#!/bin/sh

for directory in *
do
  if [ -d "$directory" ] && [ -f "$directory/Makefile" ]
  then
    cd "$directory" && make clean && cd ..
  fi
done
