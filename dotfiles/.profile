# ~/.profile: executed by the command interpreter for login shells

# add all directories in ~/.local/bin to PATH
paths="$(find ~/.local/bin -type d -printf %p:)"
export PATH=$PATH:${paths%%:}

# default programs
export EDITOR="vim"
export TERMINAL="st"
export BROWSER="firefox"

[ -f $HOME/.profile_local ] && source $HOME/.profile_local

# start ui
ssh-agent startx
