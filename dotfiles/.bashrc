###############################################################################
############################## global settings ################################
###############################################################################

# If not running interactively, don't do anything
case $- in
  *i*) ;;
  *) return ;;
esac

# source global definitions
[ -f /etc/bashrc ] && source /etc/bashrc

# don't put duplicate lines or lines starting with space in the history.
HISTCONTROL=ignoreboth
# history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=2048
HISTFILESIZE=4096

# append to the history file, don't overwrite it
shopt -s histappend

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set a fancy prompt (non-color, unless we know we "want" color)
[ "$TERM" = "xterm-color" ] && color_prompt=yes

# enable programmable completion features
if ! shopt -oq posix
then
  if [ -f /usr/share/bash-completion/bash_completion ]
  then
    source /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]
  then
    source /etc/bash_completion
  fi
fi

# prompt
export PS1="\[\e[1m\]\u@\h:\w\\$\[\e[0m\] "

###############################################################################
############################## exports ########################################
###############################################################################

export EDITOR="vim"
export PAGER="less"
export GPG_TTY="$(tty)"

###############################################################################
############################## aliases ########################################
###############################################################################

alias \
  rm='rm -iv' \
  cp='cp -iv' \
  mv='mv -iv' \
  mkdir='mkdir -p' \
  v='vim' \
  rrm='rm -rf' \
  mrm='shred -n 7 -u' \
  ..='cd ..' \
  la='LC_COLLATE=C ls -aFhHlq -1 --group-directories-first --color=always' \
  l='LC_COLLATE=C ls -aFhHlq -1 --group-directories-first --color=always'

[ -f "$HOME/.bash_aliases" ] && source "$HOME/.bash_aliases"

###############################################################################
############################## functions ######################################
###############################################################################

# make directory and cd into it
function mkcd() { mkdir -p "$1" && cd "$1"; }

# archive creator
function compress() {
  [ $# -eq 0 -o "$1" = '--help' ] \
    &&  echo -e "Usage:\n  compress ARCHIVE FILES..." \
    && return 0
  archive="$1"
  shift 1
  files="$@"
  case "$archive" in
    *.zip) zip -r "$archive" $files ;;
    *.tar) tar -cvf "$archive" $files ;;
    *.tar.gz|*.tgz) tar -czvf "$archive" $files ;;
    *.tar.bz2|*.tbz2) tar -cjvf "$archive" $files ;;
    *) echo "invalid argument" >&2 && return 1
  esac
  return 0
}

# archive extractor
function extract() {
  [ $# -eq 0 -o "$1" = '--help' ] \
    && echo -e "Usage:\n  extract ARCHIVE OUTPUT_DIR" \
    && return 0
  archive="$1"
  output_dir="$(mktemp -d -p .)"
  case "$archive" in
    *.tar) tar -xvf "$archive" -C "$output_dir" ;;
    *.tar.gz|*.tgz) tar -xzvf "$archive" -C "$output_dir" ;;
    *.tar.bz2|*.tbz2) tar -xjvf "$archive" -C "$output_dir" ;;
    *.tar.zst|*.tar.zstd) tar -I zstd -xvf "$archive" -C "$output_dir" ;;
    *.tar.lmza|*.tlmza) tar --lzma -xvf "$archive" -C "$output_dir" ;;
    *.tar.xz|*.txz) tar -xvf "$archive" -C "$output_dir" ;;
    *.gz) gunzip -c "$archive" > "$output_dir"/"${archive%.gz}" ;;
    *.bz2) bunzip2 -c "$archive" > "$output_dir"/"${archive%.bz2}" ;;
    *.xz) unxz -c "$archive" > "$output_dir"/"${archive%.xz}" ;;
    *.lzma) unlzma -c "$archive" > "$output_dir"/"${archive%.lzma}" ;;
    *.zip) unzip -d "$output_dir" "$archive" ;;
    *.rar) unrar x "$archive" "$output_dir" ;;
    *.7z) 7z x "$archive" -o "$output_dir" ;;
    *) echo "invalid argument" >&2 && return 1
  esac
  [ $? -ne 0 ] \
    && echo "failed to extract \`$archive'" >&2 \
    && return 1
  echo "archive extracted in $output_dir"
  return 0
}

###############################################################################
################################# misc ########################################
###############################################################################

# git prompt
command -v git > /dev/null 2>&1 && {
  [ -f "$HOME/.config/git/git-prompt.sh" ] \
    && source "$HOME/.config/git/git-prompt.sh" \
    && export PS1="\[\e[1m\]\u@\h:\w\$(__git_ps1 ' (%s) ')\\$\[\e[0m\] "
}

# local things
[ -f "$HOME/.bash_local" ] && source "$HOME/.bash_local"
