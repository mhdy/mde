#!/bin/sh

PROGRAM_NAME="$(basename "$0")"

EXIT_SUCCESS=0
EXIT_FAILURE=1

v="$(curl -s "https://api.github.com/repos/junegunn/fzf/releases/latest" | \
  jq '.tag_name' | \
  tr -d '"')"

echo "getting binary..."
wget -q "https://github.com/junegunn/fzf/releases/download/$v/fzf-$v-linux_amd64.tar.gz" -O fzf.tar.gz
[ $? -ne 0 ] \
  && echo "cannot download version $v" >&2 \
  && exit $EXIT_FAILURE
tar -xzf fzf.tar.gz

echo "getting man page..."
wget -q "https://raw.githubusercontent.com/junegunn/fzf/$v/man/man1/fzf.1" -O fzf.1

echo "getting bash completion and key bindings files..."
wget -q "https://raw.githubusercontent.com/junegunn/fzf/$v/shell/key-bindings.bash" -O key-bindings.bash
wget -q "https://raw.githubusercontent.com/junegunn/fzf/$v/shell/completion.bash" -O completion.bash

exit $EXIT_SUCCESS
